/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_CONSTELLATION_DECODER_CF_IMPL_H
#define INCLUDED_ELEN90089_CONSTELLATION_DECODER_CF_IMPL_H

#include <elen90089/constellation_decoder_cf.h>
#include <gnuradio/digital/constellation.h>
#include <pmt/pmt.h>

namespace gr {
namespace elen90089 {

class constellation_decoder_cf_impl : public constellation_decoder_cf
{
private:
    int d_bps;
    bool d_soft_decisions;
    pmt::pmt_t d_length_tag_key;
    
    std::map<int, digital::constellation_sptr> d_constel;

public:
    constellation_decoder_cf_impl(int bps,
                                  bool soft_decisions = true,
                                  const std::string& length_tag_name = "");

    ~constellation_decoder_cf_impl();

    int bps() const override;

    void set_bps(int bps) override;

    bool soft_decisions() const override;

    void set_soft_decisions(bool soft_decisions) override;

    void forecast(int noutput_items,
                  gr_vector_int &ninput_items_required);

    int general_work(int noutput_items,
                     gr_vector_int &ninput_items,
                     gr_vector_const_void_star &input_items,
                     gr_vector_void_star &output_items);
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_CONSTELLATION_DECODER_CF_IMPL_H */

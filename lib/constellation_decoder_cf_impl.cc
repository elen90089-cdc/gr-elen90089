/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "constellation_decoder_cf_impl.h"


namespace gr {
namespace elen90089 {

constellation_decoder_cf::sptr constellation_decoder_cf::make(int bps,
                                                              bool soft_decisions,
                                                              const std::string& length_tag_name)
{
    return gnuradio::make_block_sptr<constellation_decoder_cf_impl>(
        bps, soft_decisions, length_tag_name);
}

constellation_decoder_cf_impl::constellation_decoder_cf_impl(int bps,
                                                             bool soft_decisions,
                                                             const std::string& length_tag_name)
    : gr::block("constellation_decoder_cf",
                gr::io_signature::make(1, 1, sizeof(gr_complex)),
                gr::io_signature::make(1, 1, sizeof(float))),
      d_soft_decisions(soft_decisions),
      d_length_tag_key(pmt::PMT_NIL)
{
    set_bps(bps);
    if(!length_tag_name.empty()) {
        d_length_tag_key = pmt::intern(length_tag_name);
    }

    // manually propagate tags
    set_tag_propagation_policy(TPP_DONT);

    // create constellation maps
    d_constel[1] = digital::constellation_bpsk::make();
    d_constel[2] = digital::constellation_calcdist::make( // normalized QPSK
        {gr_complex(-1, -1), gr_complex( 1, -1),
         gr_complex(-1,  1), gr_complex( 1,  1)},
        {0x0, 0x1, 0x2, 0x3}, 4, 1,
        digital::constellation::POWER_NORMALIZATION);
    d_constel[3] = digital::constellation_8psk::make();
    d_constel[4] = digital::constellation_16qam::make();
}

constellation_decoder_cf_impl::~constellation_decoder_cf_impl() { }

int constellation_decoder_cf_impl::bps() const 
{
    return d_bps;
}

void constellation_decoder_cf_impl::set_bps(int bps)
{
    assert(bps > 0 && bps < 5);
    d_bps = bps;
}

bool constellation_decoder_cf_impl::soft_decisions() const 
{
    return d_soft_decisions;
}

void constellation_decoder_cf_impl::set_soft_decisions(bool soft_decisions)
{
    d_soft_decisions = soft_decisions;
}

void constellation_decoder_cf_impl::forecast(int noutput_items,
                                             gr_vector_int &ninput_items_required)
{
    ninput_items_required[0] = noutput_items/d_bps;
}

int constellation_decoder_cf_impl::general_work(int noutput_items,
                                                gr_vector_int &ninput_items,
                                                gr_vector_const_void_star &input_items,
                                                gr_vector_void_star &output_items)
{
    auto in = static_cast<const gr_complex*>(input_items[0]);
    auto out = static_cast<float*>(output_items[0]);    
    int nin = std::min(ninput_items[0], noutput_items/d_bps);

    // get bps tags in window
    std::vector<gr::tag_t> tags;
    get_tags_in_window(tags, 0, 0, nin, pmt::mp("bps"));

    // look for change in bps in this window
    int bps_new = -1;
    int nread = nitems_read(0);
    for(gr::tag_t tag : tags) {
        int value = pmt::to_long(tag.value);
        if(value != d_bps) {
            bps_new = value;
            nin = tag.offset - nread;
            break;
        }
    }
    tags.clear();

    // decoding with current constellation
    digital::constellation_sptr constel = d_constel[d_bps];
    if (d_soft_decisions) {
        std::vector<float> bits;
        for(int i = 0; i < nin; i++) {
            bits = constel->soft_decision_maker(in[i]);
            for (size_t j = 0; j < bits.size(); j++) {
                out[d_bps*i + j] = bits[j];
            }
        }
    } else { // hard decisions
        for(int i = 0; i < nin; i++) {
            int bits = constel->decision_maker(&in[i]);
            for (size_t j = 0; j < d_bps; j++) {
                int bit = 0x1 & (bits >> (d_bps - j - 1));
                out[d_bps*i + j] = (float)bit;
            }
        }
    }
    int nout = d_bps*nin;

    // manually propagate tags in window to safely handle changes in 
    // interpolation rate (bps)
    get_tags_in_window(tags, 0, 0, nin);
    int nwritten = nitems_written(0);
    for(gr::tag_t tag : tags) {
        int offset = d_bps*(tag.offset - nread) + nwritten;

        pmt::pmt_t value = tag.value;
        if(tag.key == d_length_tag_key) {
            int length = pmt::to_long(value);
            value = pmt::from_long(d_bps*length);
        }

        add_item_tag(0, offset, tag.key, value, tag.srcid);
    }

    consume_each(nin);

    if(bps_new > 0)
        set_bps(bps_new);

    return nout;
}

} /* namespace elen90089 */
} /* namespace gr */

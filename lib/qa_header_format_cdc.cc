

#include <elen90089/header_format_cdc.h>
#include <gnuradio/attributes.h>
#include <gnuradio/blocks/unpack_k_bits.h>
#include <volk/volk_alloc.hh>
#include <boost/test/unit_test.hpp>

namespace gr {
namespace elen90089 {

BOOST_AUTO_TEST_CASE(test_cdc_format)
{
    static const int N = 4;
    std::string ac = "10110010"; // 0xB2
    std::vector<unsigned char> data{ 0x01, 0x02, 0x03, 0x04 };
    int threshold = 3;
    header_format_cdc::sptr hdr_format;
    hdr_format = header_format_cdc::make(ac, threshold, 1);

    int bps = 2;
    pmt::pmt_t output;
    pmt::pmt_t info = pmt::make_dict();
    info = pmt::dict_add(info, pmt::mp("bps"), pmt::from_long(bps));

    bool ret = hdr_format->format(N, data.data(), output, info);
    size_t length = pmt::length(output);

    BOOST_REQUIRE(ret);
    BOOST_REQUIRE_EQUAL(length, hdr_format->header_nbytes());
    BOOST_REQUIRE_EQUAL(8*length, hdr_format->header_nbits());

    // Test access code - 0xb2
    unsigned char h0 = pmt::u8vector_ref(output, 0);
    BOOST_REQUIRE_EQUAL(0xb2, (int)h0);

    // Test counter - 0x0000
    unsigned char h1 = pmt::u8vector_ref(output, 1);
    unsigned char h2 = pmt::u8vector_ref(output, 2);
    BOOST_REQUIRE_EQUAL(0x00, (int)h1);
    BOOST_REQUIRE_EQUAL(0x00, (int)h2);

    // Test packet length - 0x0004
    unsigned char h3 = pmt::u8vector_ref(output, 3);
    unsigned char h4 = pmt::u8vector_ref(output, 4);
    BOOST_REQUIRE_EQUAL(0x00, (int)h3);
    BOOST_REQUIRE_EQUAL(0x04, (int)h4);

    // Test bits/symbol - 0x02
    unsigned char h5 = pmt::u8vector_ref(output, 5);
    BOOST_REQUIRE_EQUAL(0x02, (int)h5);

    // Test CRC - 0x9c
    unsigned char h6 = pmt::u8vector_ref(output, 6);
    BOOST_REQUIRE_EQUAL(0x9c, (int)h6);
}

BOOST_AUTO_TEST_CASE(test_cdc_parse)
{
    static const int nbytes = 11;
    static const int nbits = 8 * nbytes;
    volk::vector<unsigned char> bytes(nbytes);
    volk::vector<unsigned char> bits(nbits);

    int index = 0;
    bytes[index +  0] = 0xb2; // access code
    bytes[index +  1] = 0x00; // counter
    bytes[index +  2] = 0x00;
    bytes[index +  3] = 0x00; // pkt len
    bytes[index +  4] = 0x04; 
    bytes[index +  5] = 0x02; // bps
    bytes[index +  6] = 0x9c; // crc8
    bytes[index +  7] = 0x01; // payload
    bytes[index +  8] = 0x02;
    bytes[index +  9] = 0x03;
    bytes[index + 10] = 0x04;

    blocks::kernel::unpack_k_bits unpacker = blocks::kernel::unpack_k_bits(8);
    unpacker.unpack(bits.data(), bytes.data(), nbytes);

    uint8_t expected_bps = 2;
    std::string ac = "10110010";
    header_format_cdc::sptr hdr_format = header_format_cdc::make(ac, 0, 1);

    int count = 0;
    std::vector<pmt::pmt_t> info;
    bool ret = hdr_format->parse(nbits, bits.data(), info, count);

    BOOST_REQUIRE(ret);
    BOOST_REQUIRE_EQUAL((size_t)1, info.size());

    pmt::pmt_t dict = info[0];
    int counter = pmt::to_long(
        pmt::dict_ref(dict, pmt::intern("counter"), pmt::from_long(-1)));
    int payload_syms = pmt::to_long(
        pmt::dict_ref(dict, pmt::intern("payload symbols"), pmt::from_long(-1)));
    int bps = pmt::to_long(
        pmt::dict_ref(dict, pmt::intern("bps"), pmt::from_long(-1)));

    int hdr_bits = (int)hdr_format->header_nbits();
    int expected_bits = nbits - hdr_bits;
    BOOST_REQUIRE_EQUAL(0, counter);
    BOOST_REQUIRE_EQUAL(expected_bits, payload_syms * bps);
    BOOST_REQUIRE_EQUAL(expected_bps, (uint8_t)bps);
}

} /* namespace elen90089 */
} /* namespace gr */


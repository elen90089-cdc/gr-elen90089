/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_CORR_EST_CC_IMPL_H
#define INCLUDED_ELEN90089_CORR_EST_CC_IMPL_H

#include <elen90089/corr_est_cc.h>
#include <gnuradio/filter/fft_filter.h>

using namespace gr::filter;

namespace gr {
namespace elen90089 {

class corr_est_cc_impl : public corr_est_cc
{
private:
    pmt::pmt_t d_src_id;

    std::vector<gr_complex> d_sequence;
    float d_threshold;
    unsigned int d_mark_delay;

    kernel::fft_filter_ccc d_filter;
    volk::vector<gr_complex> d_corr;
    volk::vector<float> d_corr_mag;
    volk::vector<float> d_y_mag;
    float d_scale;
    float d_y_accum;
    int d_skip;

    static constexpr int s_nitems = 24*1024;

    double estimate_freq_offset(const gr_complex* samples);

public:
    corr_est_cc_impl(const std::vector<gr_complex>& sequence,
                     float threshold = 0.5,
                     unsigned int mark_delay=0);
  
    ~corr_est_cc_impl() override;

    std::vector<gr_complex> sequence() const override;
    void set_sequence(const std::vector<gr_complex>& sequence) override;

    float threshold() const override;
    void set_threshold(float threshold) override;

    unsigned int mark_delay() const override;
    void set_mark_delay(unsigned int mark_delay) override;

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items) override;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_CORR_EST_CC_IMPL_H */

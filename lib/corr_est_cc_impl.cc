/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include <gnuradio/math.h>
#include <volk/volk.h>
#include "corr_est_cc_impl.h"

namespace gr {
namespace elen90089 {

corr_est_cc::sptr corr_est_cc::make(const std::vector<gr_complex>& sequence,
                                    float threshold,
                                    unsigned int mark_delay)
{
    return gnuradio::make_block_sptr<corr_est_cc_impl>(
        sequence, threshold, mark_delay);
}

corr_est_cc_impl::corr_est_cc_impl(const std::vector<gr_complex>& sequence,
                                   float threshold,
                                   unsigned int mark_delay)
    : gr::sync_block("corr_est_cc",
                     gr::io_signature::make(1, 1, sizeof(gr_complex)),
                     gr::io_signature::make2(1, 2, sizeof(gr_complex), sizeof(float))),
      d_src_id(pmt::intern(alias())),
      d_sequence(sequence.size()),
      d_threshold(threshold),
      d_mark_delay(mark_delay),
      d_filter(1, sequence),
      d_corr(s_nitems),
      d_corr_mag(s_nitems),
      d_y_mag(s_nitems + sequence.size()),
      d_y_accum(0),
      d_skip(0)
{
    set_max_noutput_items(s_nitems);
    set_sequence(sequence);

    //message_port_register_out(pmt::mp("cfo"));
}

corr_est_cc_impl::~corr_est_cc_impl() {}

std::vector<gr_complex> corr_est_cc_impl::sequence() const
{
    return d_sequence;
}

void corr_est_cc_impl::set_sequence(const std::vector<gr_complex>& sequence)
{
    gr::thread::scoped_lock lock(d_setlock);

    d_sequence = sequence;

    // create time-reversed conjugate of training sequence for filtering
    std::vector<gr_complex> taps(sequence.size());
    std::reverse_copy(sequence.begin(), sequence.end(), taps.begin()); 
    d_scale = 0.0;
    for (size_t i = 0; i < taps.size(); i++) {
        taps[i] = std::conj(taps[i]);
        d_scale += std::norm(taps[i]);
    }
    d_scale = 1.0 / sqrt(d_scale);
    for (size_t i = 0; i < taps.size(); i++) {
        taps[i] *= d_scale;
    }

    // set taps and block output multiple to FFT kernel's internal nsamples
    const int nsamples = d_filter.set_taps(taps);
    set_output_multiple(nsamples);

    // keep a history of the length of the sync word to delay for tagging
    set_history(d_sequence.size());
    declare_sample_delay(0, sequence.size());
    declare_sample_delay(1, 0);
}

float corr_est_cc_impl::threshold() const
{
    return d_threshold;
}

void corr_est_cc_impl::set_threshold(float threshold)
{
    gr::thread::scoped_lock lock(d_setlock);
    d_threshold = threshold;
}

unsigned int corr_est_cc_impl::mark_delay() const
{
    return d_mark_delay;
}

void corr_est_cc_impl::set_mark_delay(unsigned int mark_delay)
{
    gr::thread::scoped_lock lock(d_setlock);
    d_mark_delay = mark_delay;
}

double corr_est_cc_impl::estimate_freq_offset(const gr_complex* samples)
{
    double accum = 0;

    gr_complex y_p = samples[0] / d_sequence[0];
    for (int i = 1; i < d_sequence.size(); i++) {
        gr_complex y = samples[i] / d_sequence[i];
        gr_complex p = y * conj(y_p);
        accum += fast_atan2f(p.imag(), p.real());
        y_p = y;
    }

    return (accum / (d_sequence.size() - 1));
}

int corr_est_cc_impl::work(int noutput_items,
                           gr_vector_const_void_star &input_items,
                           gr_vector_void_star &output_items)
{
    auto in = static_cast<const gr_complex*>(input_items[0]);
    auto out = static_cast<gr_complex*>(output_items[0]);

    float* corr_mag;
    if (output_items.size() > 1) {
        corr_mag = static_cast<float*>(output_items[1]);
    } else {
        corr_mag = d_corr_mag.data();
    }

    unsigned int hist_len = history() - 1;

    float threshold;
    {
        gr::thread::scoped_lock lock(d_setlock);
        threshold = d_threshold;
    }

    // correlate input samples with training sequence
    d_filter.filter(noutput_items, &in[hist_len], d_corr.data());

    // calculate squared magnitude of correlation result
    volk_32fc_magnitude_squared_32f(corr_mag, d_corr.data(), noutput_items);

    // calculate squared magnitued of input samples for normalization
    volk_32fc_magnitude_squared_32f(d_y_mag.data(), in, noutput_items + history());

    // search for correlation peaks above threshold
    for (int i = 0; i < noutput_items; i++) {
        // add sample to moving average
        d_y_accum += d_y_mag[hist_len + i];

        // normalize cross-correlation
        corr_mag[i] /= d_y_accum;

        // drop oldest sample from moving average
        d_y_accum -= d_y_mag[i]; 

        if (d_skip > 0) {
            d_skip--;
            continue;
        }

        if (corr_mag[i] > d_threshold) {
            // coarse frequency estimate
            //double freq_est = estimate_freq_offset(&in[i]);
            
            // single-tap (inverse) channel estimate
            gr_complex chan_est = d_corr[i] * d_scale;
            chan_est = conj(chan_est) / (chan_est*conj(chan_est));
            
            // time phase estimate - center-of-mass (3 samples)
            float m1 = corr_mag[i-1];
            float m2 = corr_mag[i+0];
            float m3 = corr_mag[i+1] / (d_y_accum + d_y_mag[hist_len+i+1]);
            double nom = m1 + 2*m2 + 3*m3;
            double den = m1 + m2 + m3;
            double time_est = (nom/den) - 2.0;

            // send frequency correction message
            int offset = nitems_written(0) + i;
            //pmt::pmt_t cmd = pmt::make_dict();
            //cmd = pmt::dict_add(cmd,
            //                    pmt::mp("offset"),
            //                    pmt::from_long(offset));
            //cmd = pmt::dict_add(cmd,
            //                    pmt::mp("inc"),
            //                    pmt::from_double(-freq_est));
            //message_port_pub(pmt::mp("cfo"), cmd);

            // tag output
            for(int ch = 0; ch < output_items.size(); ch++) {
                add_item_tag(ch, offset,
                             pmt::intern("corr_start"),
                             pmt::from_double(corr_mag[i]),
                             d_src_id);
                add_item_tag(ch, offset + d_mark_delay,
                             pmt::intern("corr_est"),
                             pmt::from_double(corr_mag[i]),
                             d_src_id);
                add_item_tag(ch, offset + d_mark_delay,
                             pmt::intern("chan_est"),
                             pmt::from_complex(chan_est),
                             d_src_id);
                add_item_tag(ch, offset + d_mark_delay,
                             pmt::intern("phase_est"), // reset Costas Loop
                             pmt::from_double(0.0),
                             d_src_id);
                add_item_tag(ch, offset + d_mark_delay,
                             pmt::intern("time_est"),
                             pmt::from_double(time_est),
                             d_src_id);
            }

            // skip remaining symbols in sequence
            d_skip = d_sequence.size() - 1;
        }
    }

    // pass through input samples but with delay of history()
    memcpy(out, &in[0], sizeof(gr_complex)*noutput_items);

    return noutput_items;
}

} /* namespace elen90089 */
} /* namespace gr */

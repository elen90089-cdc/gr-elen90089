/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_DSA_PU_SCENARIO_IMPL_H
#define INCLUDED_ELEN90089_DSA_PU_SCENARIO_IMPL_H

#include <elen90089/dsa_pu_scenario_cc.h>
#include <random>

namespace gr {
namespace elen90089 {

class dsa_pu_scenario_cc_impl : public dsa_pu_scenario_cc
{
private:
    int d_scenario;
    bool d_random;
    int d_samp_rate;
    float d_duration_ms;

    std::default_random_engine d_engine;
    std::uniform_int_distribution<int>* d_uniform = nullptr;
    std::vector<bool> d_active;
    int d_samp_cnt = 0;

    void update_scenario(int n_chan);

public:
    dsa_pu_scenario_cc_impl(int scenario,
                            bool random,
                            int seed,
                            int samp_rate,
                            float duration_ms);
    ~dsa_pu_scenario_cc_impl();

    void set_scenario(int scenario) { d_scenario = scenario; };
    
    int get_scenario(void) { return d_scenario; };
    
    void set_random(bool random) { d_random = random; };
    
    bool get_random(void) { return random; };
    
    void set_duration_ms(float duration_ms) { d_duration_ms = duration_ms; };
    
    float get_duration_ms(void) { return d_duration_ms; };

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);

};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_DSA_PU_SCENARIO_IMPL_H */

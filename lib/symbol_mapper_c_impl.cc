/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "symbol_mapper_c_impl.h"

namespace gr {
namespace elen90089 {

symbol_mapper_c::sptr symbol_mapper_c::make(constel_t constel_header,
                                            const std::string& tsb_tag_key)
{
    return gnuradio::make_block_sptr<symbol_mapper_c_impl>(
        constel_header, tsb_tag_key);
}

symbol_mapper_c_impl::symbol_mapper_c_impl(constel_t constel_header,
                                           const std::string& tsb_tag_key)
    : gr::tagged_stream_block("symbol_mapper_c",
                              gr::io_signature::make(0, 0, 0),
                              gr::io_signature::make(1, 1, sizeof(gr_complex)),
                              tsb_tag_key),
      d_bps_header(0),
      d_bps_payload(0),
      d_len_header(0),
      d_len_payload(0)
{
    // register message ports to receive header and payload pdus
    message_port_register_in(pmt::mp("hdr"));
    message_port_register_in(pmt::mp("pld"));

    // set header bps
    set_constel_header(constel_header);

    // create constellation objects
    d_bpsk  = digital::constellation_bpsk::make();
    d_8psk  = digital::constellation_8psk::make();
    d_16qam = digital::constellation_16qam::make();

    // digital::constellation_qpsk is unnormalized so create manually
    d_qpsk = digital::constellation_calcdist::make(
        {gr_complex(-1, -1), gr_complex( 1, -1),
         gr_complex(-1,  1), gr_complex( 1,  1)}, // constel
        {0x0, 0x1, 0x2, 0x3},                     // pre_diff_code
        4,                                        // rotational_symmetry
        1,                                        // dimensionality
        digital::constellation::POWER_NORMALIZATION);
}

symbol_mapper_c_impl::~symbol_mapper_c_impl() { }

symbol_mapper_c::constel_t symbol_mapper_c_impl::constel_header() const
{
    constel_t constel;

    switch(d_bps_header) {
    case 1: constel = CONSTEL_BPSK; break;
    case 2: constel = CONSTEL_QPSK; break;
    case 3: constel = CONSTEL_8PSK; break;
    case 4: constel = CONSTEL_16QAM; break;
    }

    return constel;
}

void symbol_mapper_c_impl::set_constel_header(constel_t constel)
{
    switch(constel) {
    case CONSTEL_BPSK:  d_bps_header = 1; break;
    case CONSTEL_QPSK:  d_bps_header = 2; break;
    case CONSTEL_8PSK:  d_bps_header = 3; break;
    case CONSTEL_16QAM: d_bps_header = 4; break;
    }
}

int symbol_mapper_c_impl::calculate_output_stream_length(const gr_vector_int &ninput_items)
{
    int noutput_items = 0;

    // get next header pdu
    if (d_len_header == 0) {
        pmt::pmt_t msg(delete_head_nowait(pmt::mp("hdr")));

        if (msg.get() != nullptr) {
            assert(!pmt::is_pair(msg));

            d_metadata = pmt::car(msg);
            d_bps_payload = pmt::to_long(pmt::dict_ref(
                d_metadata, pmt::mp("bps"), pmt::from_long(1)));

            d_header = pmt::cdr(msg);
            d_len_header = pmt::blob_length(d_header);
        }
    }

    // get next payload pdu
    if (d_len_payload == 0) {
        pmt::pmt_t msg(delete_head_nowait(pmt::mp("pld")));

        if (msg.get() != nullptr) {
            d_payload = pmt::cdr(msg);
            d_len_payload = pmt::blob_length(d_payload);
        }
    }

    if (d_len_header > 0 && d_len_payload > 0) {
        noutput_items += ((d_len_header << 3) + (d_bps_header - 1)) / d_bps_header;
        noutput_items += ((d_len_payload << 3) + (d_bps_payload - 1)) / d_bps_payload;
    }

    return noutput_items;
}

void symbol_mapper_c_impl::map_to_symbols(const uint8_t* bytes,
                                          int nbytes,
                                          gr_complex* symbols,
                                          int bps)
{
    digital::constellation_sptr constel;

    switch (bps) {
    case 1: constel = d_bpsk; break;
    case 2: constel = d_qpsk; break;
    case 3: constel = d_8psk; break;
    case 4: constel = d_16qam; break;
    }

    int nsymbols = (nbytes << 3) / bps;
    int index = 0;
    for (int i = 0; i < nsymbols; i++) {
        // MSB unpacking of bps bits from input bytes
        int x = 0;
        for (unsigned int j = 0; j < bps; j++, index++) {
            int bit = 0x1 & (bytes[index>>3] >> (7 - (index & 0x7)));
            x = (x << 1) | bit;
        }

        constel->map_to_points(x, symbols++);
    }

    // zero pad and map left over bits
    if (index & 0x7 != 0) {
        int x = bytes[index>>3] << (bps - (8 - (index & 0x7)));
        x &= (0x1 << bps) - 1;

        constel->map_to_points(x, symbols);
    }
}

int symbol_mapper_c_impl::work(int noutput_items,
                               gr_vector_int &ninput_items,
                               gr_vector_const_void_star &input_items,
                               gr_vector_void_star &output_items)
{
    auto out = static_cast<gr_complex*>(output_items[0]);

    if (d_len_header == 0 || d_len_payload == 0) {
        return 0;
    }

    int nout = ((d_len_header << 3) + (d_bps_header - 1)) / d_bps_header;
    nout += ((d_len_payload << 3) + (d_bps_payload - 1)) / d_bps_payload;

    assert (noutput_items >= nout);

    // modulate header bytes
    size_t io(0);
    const uint8_t* bytes = static_cast<const uint8_t*>(pmt::uniform_vector_elements(d_header, io));
    map_to_symbols(bytes, d_len_header, out, d_bps_header);
    out += ((d_len_header << 3) + (d_bps_header - 1)) / d_bps_header;

    // modulate payload bytes
    bytes = static_cast<const uint8_t*>(pmt::uniform_vector_elements(d_payload, io));
    map_to_symbols(bytes, d_len_payload, out, d_bps_payload);

    // add start of burst and end of burst tags
    int offset = nitems_written(0);
    //add_item_tag(0, offset,
    //             pmt::mp("tx_sob"),
    //             pmt::PMT_T,
    //             alias_pmt());
    //add_item_tag(0, offset + nout - 1,
    //             pmt::mp("tx_eob"),
    //             pmt::PMT_T,
    //             alias_pmt());

    // convert header metadata to stream tags
    pmt::pmt_t items(pmt::dict_items(d_metadata));
    for (size_t i = 0; i < pmt::length(items); i++) {
        pmt::pmt_t item(pmt::nth(i, items));
        add_item_tag(0, offset,
                     pmt::car(item),
                     pmt::cdr(item),
                     alias_pmt());
    }

    d_len_header = 0;
    d_len_payload = 0;

    return nout;
}

} /* namespace elen90089 */
} /* namespace gr */

/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include <volk/volk_alloc.hh>
#include <elen90089/header_format_cdc.h>

namespace gr {
namespace elen90089 {

header_format_cdc::sptr
header_format_cdc::make(const std::string& access_code,
                        int threshold,
                        int bps)
{
    return header_format_cdc::sptr(
        new header_format_cdc(access_code, threshold, bps));
}

header_format_cdc::header_format_cdc(const std::string& access_code,
                                     int threshold,
                                     int bps)
    : header_format_default(access_code, threshold, bps),
      d_counter(0)
{
    assert(bps > 0 && bps < 5);
}

header_format_cdc::~header_format_cdc() {}

bool header_format_cdc::format(int nbytes_in,
                               const unsigned char* input,
                               pmt::pmt_t& output,
                               pmt::pmt_t& info)

{
    // Creating the output pmt copies data; free our own here when done.
    volk::vector<uint8_t> bytes_out(header_nbytes());

    // update bps from metadata dict if present
    uint8_t bps = (uint8_t)pmt::to_long(pmt::dict_ref(info,
                                                      pmt::mp("bps"),
                                                      pmt::from_long(d_bps)));
    assert(bps > 0 && bps < 5);

    // calculate 8-bit CRC
    d_crc_impl.reset();
    d_crc_impl.process_bytes((void const*)&d_counter, 2);
    d_crc_impl.process_bytes((void const*)&nbytes_in, 2);
    d_crc_impl.process_bytes((void const*)&bps,       1);
    uint8_t crc = d_crc_impl();

    // create header
    digital::header_buffer header(bytes_out.data());
    header.add_field64(d_access_code, d_access_code_len);
    header.add_field16(d_counter);
    header.add_field16(nbytes_in);
    header.add_field8 (bps);
    header.add_field8 (crc);

    d_counter++;

    // Package output data into a PMT vector
    output = pmt::init_u8vector(header_nbytes(), bytes_out.data());

    return true;
}

size_t header_format_cdc::header_nbits() const
{
    return d_access_code_len + 8 * 3 * sizeof(uint16_t);
}

bool header_format_cdc::header_ok()
{
    uint16_t counter  = d_hdr_reg.extract_field16(0);
    uint16_t pktlen   = d_hdr_reg.extract_field16(16);
    uint8_t  bps      = d_hdr_reg.extract_field8(32);
    uint8_t  crc_rcvd = d_hdr_reg.extract_field8(40);

    // check crc8
    d_crc_impl.reset();
    d_crc_impl.process_bytes((void const*)&counter, 2);
    d_crc_impl.process_bytes((void const*)&pktlen,2);
    d_crc_impl.process_bytes((void const*)&bps, 1);
    uint8_t crc_clcd = d_crc_impl();

    return (crc_rcvd == crc_clcd);
}

int header_format_cdc::header_payload()
{
    uint16_t counter = d_hdr_reg.extract_field16(0);
    uint16_t pktlen  = d_hdr_reg.extract_field16(16);
    uint8_t bps      = d_hdr_reg.extract_field8(32);

    d_bps = bps;

    d_info = pmt::make_dict();
    d_info = pmt::dict_add(
        d_info, pmt::intern("counter"), pmt::from_long(counter));
    d_info = pmt::dict_add(
        d_info, pmt::intern("payload symbols"), pmt::from_long(8*pktlen/d_bps));
    d_info = pmt::dict_add(
        d_info, pmt::intern("bps"), pmt::from_long(bps));
    // hack to trigger reset of Costas loop phase in payload rx chain
    // make sure CPO is removed before Costas block
    d_info = pmt::dict_add(
        d_info, pmt::intern("phase_est"), pmt::from_double(0.0));

    return static_cast<int>(pktlen);
}

} /* namespace elen90089 */
} /* namespace gr */

/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_COSTAS_LOOP_CC_IMPL_H
#define INCLUDED_ELEN90089_COSTAS_LOOP_CC_IMPL_H

#include <elen90089/costas_loop_cc.h>

namespace gr {
namespace elen90089 {

class costas_loop_cc_impl : public costas_loop_cc
{
private:
    float d_error;
    float d_noise;
    bool d_use_snr;
    int d_order;

    float phase_detector_8(gr_complex sample) const // for 8PSK
    {
        const float K = (sqrtf(2.0) - 1);
        if (fabsf(sample.real()) >= fabsf(sample.imag())) {
            return ((sample.real() > 0.0f ? 1.0f : -1.0f) * sample.imag() -
                    (sample.imag() > 0.0f ? 1.0f : -1.0f) * sample.real() * K);
        } else {
            return ((sample.real() > 0.0f ? 1.0f : -1.0f) * sample.imag() * K -
                    (sample.imag() > 0.0f ? 1.0f : -1.0f) * sample.real());
        }
    };

    float phase_detector_4(gr_complex sample) const // for QPSK
    {
        return ((sample.real() > 0.0f ? 1.0f : -1.0f) * sample.imag() -
                (sample.imag() > 0.0f ? 1.0f : -1.0f) * sample.real());
    };

    float phase_detector_2(gr_complex sample) const // for BPSK
    {
        return (sample.real() * sample.imag());
    }

    float phase_detector_snr_8(gr_complex sample) const // for 8PSK
    {
        const float K = (sqrtf(2.0) - 1.0);
        const float snr = std::norm(sample) / d_noise;
        if (fabsf(sample.real()) >= fabsf(sample.imag())) {
            return ((blocks::tanhf_lut(snr * sample.real()) * sample.imag()) -
                    (blocks::tanhf_lut(snr * sample.imag()) * sample.real() * K));
        } else {
            return ((blocks::tanhf_lut(snr * sample.real()) * sample.imag() * K) -
                    (blocks::tanhf_lut(snr * sample.imag()) * sample.real()));
        }
    };

    float phase_detector_snr_4(gr_complex sample) const // for QPSK
    {
        const float snr = std::norm(sample) / d_noise;
        return ((blocks::tanhf_lut(snr * sample.real()) * sample.imag()) -
                (blocks::tanhf_lut(snr * sample.imag()) * sample.real()));
    };

    float phase_detector_snr_2(gr_complex sample) const // for BPSK
    {
        const float snr = std::norm(sample) / d_noise;
        return blocks::tanhf_lut(snr * sample.real()) * sample.imag();
    };

public:
    costas_loop_cc_impl(float loop_bw,
                        unsigned int order,
                        bool use_snr = false);

    ~costas_loop_cc_impl() override;

    float error() const override { return d_error; };

    void handle_set_noise(pmt::pmt_t msg);

    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items) override;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_COSTAS_LOOP_CC_IMPL_H */

/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_MOE_SYMBOL_SYNC_CC_IMPL_H
#define INCLUDED_ELEN90089_MOE_SYMBOL_SYNC_CC_IMPL_H

#include <elen90089/moe_symbol_sync_cc.h>
#include <volk/volk_alloc.hh>

namespace gr {
namespace elen90089 {

class moe_symbol_sync_cc_impl : public moe_symbol_sync_cc
{
private:
    int d_nsymbols;
    uint32_t d_index;
    volk::vector<float> d_mag_sq;
    volk::vector<float> d_sum;

public:
    moe_symbol_sync_cc_impl(int sps,
                            int nsymbols);

    ~moe_symbol_sync_cc_impl();

    int nsymbols() const override;
    void set_nsymbols(int nsymbols) override;

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_MOE_SYMBOL_SYNC_CC_IMPL_H */

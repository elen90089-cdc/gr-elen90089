/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_SYMBOL_MAPPER_C_IMPL_H
#define INCLUDED_ELEN90089_SYMBOL_MAPPER_C_IMPL_H

#include <elen90089/symbol_mapper_c.h>
#include <gnuradio/digital/constellation.h>

namespace gr {
namespace elen90089 {

class symbol_mapper_c_impl : public symbol_mapper_c
{
private:
    int d_bps_header;
    int d_bps_payload;
    int d_len_header;
    int d_len_payload;

    pmt::pmt_t d_header;
    pmt::pmt_t d_payload;
    pmt::pmt_t d_metadata;

    // constellation objects
    digital::constellation_sptr d_bpsk;
    digital::constellation_sptr d_qpsk;
    digital::constellation_sptr d_8psk;
    digital::constellation_sptr d_16qam;

    void map_to_symbols(const uint8_t* bytes,
                        int nbytes,
                        gr_complex* symbols,
                        int bps);

protected:
    int calculate_output_stream_length(const gr_vector_int &ninput_items) override;

public:
    symbol_mapper_c_impl(constel_t constel_header = CONSTEL_BPSK,
                         const std::string& tsb_tag_key = "packet_len");

    ~symbol_mapper_c_impl();

    constel_t constel_header() const override;
    void set_constel_header(constel_t constel_header) override;

    int work(int noutput_items,
             gr_vector_int &ninput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_SYMBOL_MAPPER_C_IMPL_H */

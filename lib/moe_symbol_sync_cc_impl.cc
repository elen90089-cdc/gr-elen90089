/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "moe_symbol_sync_cc_impl.h"

namespace gr {
namespace elen90089 {

moe_symbol_sync_cc::sptr moe_symbol_sync_cc::make(int sps,
                                                  int nsymbols)
{
    return gnuradio::make_block_sptr<moe_symbol_sync_cc_impl>(sps, nsymbols);
}

moe_symbol_sync_cc_impl::moe_symbol_sync_cc_impl(int sps,
                                                 int nsymbols)
    : gr::sync_decimator("moe_symbol_sync_cc",
                         gr::io_signature::make(1, 1, sizeof(gr_complex)),
                         gr::io_signature::make2(1, 2, sizeof(gr_complex), sizeof(int)),
                         sps),
      d_nsymbols(nsymbols),
      d_index(0),
      d_mag_sq(sps, 0.0),
      d_sum(sps, 0.0)
{}

moe_symbol_sync_cc_impl::~moe_symbol_sync_cc_impl() { }

int moe_symbol_sync_cc_impl::nsymbols() const {
    return d_nsymbols;
}

void moe_symbol_sync_cc_impl::set_nsymbols(int nsymbols) {
    d_nsymbols = nsymbols;
}

int moe_symbol_sync_cc_impl::work(int noutput_items,
                                  gr_vector_const_void_star &input_items,
                                  gr_vector_void_star &output_items)
{
    auto in = static_cast<const gr_complex*>(input_items[0]);
    auto out0 = static_cast<gr_complex*>(output_items[0]);

    int *out1 = nullptr;
    if (output_items.size() > 1)
        out1 = static_cast<int*>(output_items[1]);

    int sps = decimation();
    auto mag_sq = d_mag_sq.data();
    auto sum = d_sum.data();

    int isymb = 0;
    while(isymb < noutput_items) {
        out0[isymb] = in[d_index];
        if(out1 != nullptr)
            out1[isymb] = static_cast<int>(d_index);

        volk_32fc_magnitude_squared_32f(mag_sq, in, sps);
        volk_32f_x2_add_32f(sum, sum, mag_sq, sps);

        if (++isymb % d_nsymbols == 0) {
            volk_32f_index_max_32u(&d_index, sum, sps);
            std::fill(d_sum.begin(), d_sum.end(), 0.0);
        }

        in += sps; // increment input one symbol period
    }

    return noutput_items;
}

} /* namespace elen90089 */
} /* namespace gr */

/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_HEADER_FORMAT_CDC_H
#define INCLUDED_ELEN90089_HEADER_FORMAT_CDC_H

#include <elen90089/api.h>
#include <gnuradio/digital/header_format_default.h>
#include <pmt/pmt.h>
#include <boost/crc.hpp>

namespace gr {
namespace elen90089 {

/*!
 * \brief Header formatter for CDC packets that adds counter, packet length,
 * payload bits/symbol, and 8-bit CRC.
 *
 * \details
 *
 * Child class of header_format_default. The \p bps field of header is updated
 * based on value found in packet metadata dictionary. Packet structure is:
 *
 *   | access_code | hdr | payload |
 *
 * Where the access_code is <= 64 bits and hdr is:
 *
 *   |  0 --  7 |  8 -- 15 |
 *   |       counter       |
 *   |       pkt len       |
 *   | bits/sym |   crc8   |
 *
 * Access code and header are formatted for network byte order (big endian)
 *
 */
class ELEN90089_API header_format_cdc : public digital::header_format_default
{
public:
    typedef std::shared_ptr<header_format_cdc> sptr;

    header_format_cdc(const std::string& access_code,
                      int threshold,
                      int bps);
    
    ~header_format_cdc() override;

    
    /*!
     * Creates a header from the access code and packet length to build an
     * output packet in the form:
     *
     *   | access code | counter | pkt len | bps | crc8 |
     *
     * \param nbytes        The length (in bytes) of the \p input payload
     * \param input         An array of unsigned chars of the packet payload
     * \param output        A pmt::u8vector with the new header prepended
     *                      onto the input data.
     * \param info          A pmt::dict containing meta data and info about
     *                      the PDU (generally from the metadata portion of the
     *                      input PDU). Data can be extracted from this for the
     *                      header formatting or inserted.
     */
    bool format(int nbytes,
                const unsigned char* input,
                pmt::pmt_t& output,
                pmt::pmt_t& info) override;

    /*!
     * Returns the length of the formatted header in bits.
     */
    size_t header_nbits() const override;

    /*!
     * Make a CDC header formatter object.
     *
     * \param access_code   Frame access code, string of 0s and 1s (<= 64 bits)
     * \param threshold     Bit error threshold in access code detection.
     * \param bps           Default payload bits per symbol. Updated based on
     *                      bps field in metadata.
     */
    static sptr make(const std::string& access_code,
                     int threshold,
                     int bps);

protected:
    uint16_t d_counter;
    boost::crc_optimal<8, 0x07, 0xFF, 0x00, false, false> d_crc_impl;

    //! Verify that the header is valid
    bool header_ok() override;

    /*! Get info from the header; return payload length and package
     *  rest of data in d_info dictionary.
     *
     * Extracts the header of the form:
     *
     *  | access code | counter | pkt len | bps | crc8 | payload |
     */
    int header_payload() override;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_HEADER_FORMAT_CDC_H */

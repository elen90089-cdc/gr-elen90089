/*
 * Copyright 2011 Free Software Foundation, Inc.
 *
 * This file was generated by gr_modtool, a tool from the GNU Radio framework
 * This file is a part of gr-elen90089
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#ifndef INCLUDED_ELEN90089_API_H
#define INCLUDED_ELEN90089_API_H

#include <gnuradio/attributes.h>

#ifdef gnuradio_elen90089_EXPORTS
#define ELEN90089_API __GR_ATTR_EXPORT
#else
#define ELEN90089_API __GR_ATTR_IMPORT
#endif

#endif /* INCLUDED_ELEN90089_API_H */

/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_CORR_EST_CC_H
#define INCLUDED_ELEN90089_CORR_EST_CC_H

#include <elen90089/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace elen90089 {

/*!
 * \brief Search for symbol sequence within sample stream via correlation
 * \ingroup elen90089
 *
 * \details
 * Input:
 * \li Stream of complex samples.
 *
 * Output:
 * \li Output stream of input complex samples delayed by sequence length
 * \li Optional second output stream providing normalized correlator output
 * 
 * Both output streams are tagged with the following stream tags: 
 * \li tag 'corr_start': normalized correlation value for peak 
 * \li tag 'corr_est':   normalized correlation value for peak
 * \li tag 'chan_est':   inverse of LLS channel estimate from training seq
 * \li tag 'phase_est':  hardcoded to zero (phase offset is handled by chan_est)
 * \li tag 'time_est':   fractional timing sample offset
 * Note: 'corr_start' tag is added at the detected peak and all other tags are
 * delayed by mark_delay samples to account for possible group delay of downstream
 * matched filter block.
 */
class ELEN90089_API corr_est_cc : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<corr_est_cc> sptr;

    /*!
     * Make a block that searchers for the \p sequence vector within input
     * sample stream and outputs stream tagged with correlation and frequency
     * offset estimate.
     *
     * \param sequence      Symbol sequence to correlate agains.
     * \param threshold     Correlation threshold to declare sequence found
     *                      (0.0 to 1.0)
     * \param mark_delay    Tag marking delay to account for possible
     *                      group delay of downstream matched filter
     */
    static sptr make(const std::vector<gr_complex>& sequence,
                     float threshold=0.5,
                     unsigned int mark_delay=0);

    virtual std::vector<gr_complex> sequence() const = 0;
    virtual void set_sequence(const std::vector<gr_complex>& sequence) = 0;

    virtual float threshold() const = 0;
    virtual void set_threshold(float threshold) = 0;

    virtual unsigned int mark_delay(void) const = 0;
    virtual void set_mark_delay(unsigned int mark_delay) = 0;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_CORR_EST_CC_H */

/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_ELEN90089_DSA_PU_SCENARIO_H
#define INCLUDED_ELEN90089_DSA_PU_SCENARIO_H

#include <elen90089/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace elen90089 {

/*!
 * \brief Implements primary user scenario control for CDC dynamic spectrum
          access project.
 * \ingroup elen90089
 *
 */
class ELEN90089_API dsa_pu_scenario_cc : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<dsa_pu_scenario_cc> sptr;

    /*!
    * \brief DSA PU Scenario Constructor
    *
    * \param scenario       Primary user scenario - bitmap of active channels,
    *                       e.g., for 4 PU channels SCENARIO = 10 = 0b1010
                            indicates channels 2 and 4 are active. Set to -1
    *                       for randomly selected scenarios.
    * \param random         Randomly select primary user scenario
    * \param seed           Seed used used in random scenario selection.
    * \param samp_rate      Sample rate of incoming data streams
    * \param duration_ms    Duration (in ms) of scenario before randomly
    *                       selecting new scenario.
    */
    static sptr make(int scenario,
                     bool random,
                     int seed,
                     int samp_rate,
                     float duration_ms);
    
    //! Set primary user scenario
    virtual void set_scenario(int scenario) = 0;

    //! Get current primary user scenario
    virtual int get_scenario(void) = 0;

    //! Set random selection of primary user scenarios
    virtual void set_random(bool random) = 0;

    //! Get random selection of primary user scenarios
    virtual bool get_random(void) = 0;
    
    //! Set duration (in ms) of random primary user scenarios
    virtual void set_duration_ms(float duration_ms) = 0;

    //! Get duration (in ms) of random primary user scenarios
    virtual float get_duration_ms(void) = 0;

};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_DSA_PU_SCENARIO_H */

/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_CONSTELLATION_DECODER_CF_H
#define INCLUDED_ELEN90089_CONSTELLATION_DECODER_CF_H

#include <elen90089/api.h>
#include <gnuradio/sync_interpolator.h>

namespace gr {
namespace elen90089 {

/*!
 * \brief CDC soft or hard decision constellation decoder
 * \ingroup elen90089
 *
 * \details
 * Decode a constellation's points from a complex space to either hard bits or
 * soft bits from soft decision LUT. The default constellation is set by
 * the bps parameter in the constructor. The constellation used is updated
 * from input stream tags with key 'bps' and value of the bits per symbol of
 * the desired constellation. The following constellations are supported:
 *
 * \li BPSK     bps = 1
 * \li QPSK     bps = 2
 * \li 8PSK     bps = 3
 * \li 16QAM    bps = 4
 *
 * Both hard and soft decision making is supported. Tags will be propagated
 * on the correct sample based on the interpolation factor of the current
 * constellation, e.g., the bps. If a length tag name is provided, the value
 * of this tag will be scaled by the current bps.
 */
class ELEN90089_API constellation_decoder_cf : virtual public gr::block
{
public:
    typedef std::shared_ptr<constellation_decoder_cf> sptr;

    /*!
     * \brief Make a CDC constellation decoder block.
     *
     * \param bps               (int) Bits per symbol of constellation
     * \param soft_decisions    (bool) Use soft decision making
     * \param length_tag_name   (string) Tag key identifying length of symbol
     *                          blocks
     */
    static sptr make(int bps,
                     bool soft_decisions = true,
                     const std::string& length_tag_name = "");

    /*!
     * \brief Returns current bits per symbol of constellation.
     */
    virtual int bps() const = 0;

    /*!
     * \brief Set bits per symbol of constellation.
     *
     * \param bps   (int) Bits per symbol of constellation
     */
    virtual void set_bps(int bps) = 0;

    /*!
     * \brief Returns whether soft decision making is used.
     */
    virtual bool soft_decisions() const = 0;

    /*!
     * \brief Set whether soft decision making is used.
     *
     * \param soft_decisions    (bool) Use soft decision making.
     */
    virtual void set_soft_decisions(bool soft_decisions) = 0;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_CONSTELLATION_DECODER_CF_H */

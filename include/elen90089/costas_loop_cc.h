/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_COSTAS_LOOP_CC_H
#define INCLUDED_ELEN90089_COSTAS_LOOP_CC_H

#include <elen90089/api.h>
#include <gnuradio/sync_block.h>
#include <gnuradio/blocks/control_loop.h>

namespace gr {
namespace elen90089 {

/*!
 * \brief <+description of block+>
 * \ingroup elen90089
 *
 */
class ELEN90089_API costas_loop_cc : virtual public gr::sync_block,
                                     virtual public gr::blocks::control_loop
{
public:
    typedef std::shared_ptr<costas_loop_cc> sptr;

    static sptr make(float loop_bw,
                     unsigned int order,
                     bool use_snr = false);

    virtual float error() const = 0;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_COSTAS_LOOP_CC_H */

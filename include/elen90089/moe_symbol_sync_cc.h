/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_MOE_SYMBOL_SYNC_CC_H
#define INCLUDED_ELEN90089_MOE_SYMBOL_SYNC_CC_H

#include <elen90089/api.h>
#include <gnuradio/sync_decimator.h>

namespace gr {
namespace elen90089 {

/*!
 * \brief Maximum output energy (MOE) symbol synchronization.
 * \ingroup elen90089
 *
 * \details
 * Input:
 * \li Complex stream of symbols oversampled by factor of sps
 *
 * Output:
 * \li Downsampled 
 * \li Optional second output stream of ints
 */
class ELEN90089_API moe_symbol_sync_cc : virtual public gr::sync_decimator
{
public:
    typedef std::shared_ptr<moe_symbol_sync_cc> sptr;

    /*!
     * \brief Make a block that selects \p sps samples per symbol period.
     * 
     * \param sps           Samples per symbol
     * \param nsymbols      Symbols in averaging period
     */
    static sptr make(int sps,
                     int nsymbols);

    virtual int nsymbols() const = 0;
    virtual void set_nsymbols(int nsymbols) = 0;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_MOE_SYMBOL_SYNC_CC_H */

/* -*- c++ -*- */
/*
 * Copyright 2022 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ELEN90089_SYMBOL_MAPPER_C_H
#define INCLUDED_ELEN90089_SYMBOL_MAPPER_C_H

#include <elen90089/api.h>
#include <gnuradio/tagged_stream_block.h>

namespace gr {
namespace elen90089 {

/*!
 * \brief Maps header and payload bytes of CDC packet to constellation symbols.
 * \ingroup elen90089
 *
 * \details
 * This block takes in the header and payload PDUs of a CDC packet, mapping
 * the respective bytes of each PDU to constellation symbol points to form
 * a modulated packet. Mapping of header and payload bytes is done using
 * different constellations. The default header constellation is BPSK and
 * but can be set via the \p constel_header parameter. The default payload
 * constellation is BPSK but can be chosen on a packet-by-packet basis by
 * setting the \p bps (bits-per-symbol) parameter in the metadata dictionary
 * of the header PDU. The following constellation types are supported for
 * both header and payload mapping:
 *
 * \li BPSK:  bps = 1
 * \li QPSK:  bps = 2
 * \li 8PSK:  bsp = 3
 * \li 16QAM: bps = 4
 *
 * This block uses asynchronous message passing interfaces to receive PDUs.
 * The input message ports are:
 *
 * \li hdr: header PDU of CDC packet with metadata dict specifying payload bps.
 * \li pld: payload PDU of CDC packet
 *
 * The block output is a complex stream of symbol constellation points with
 * the following stream tags:
 *
 * \li metadata:        first packet symbol is tagged with all entries in
 *                      header PDU metadata dictionary
 * \li \p tsb_tag_key:  first packet symbol is tagged with burst duration /
 *                      packet length in number of symbols
 * \li tx_sob:          indicates first symbol in packet/burst
 * \li tx_eob:          indicates last symbol in packet/burst
 */
class ELEN90089_API symbol_mapper_c : virtual public gr::tagged_stream_block
{
public:
    typedef std::shared_ptr<symbol_mapper_c> sptr;

    /*!
     * \brief Enum to represent constellation type.
     */
    enum constel_t {
        CONSTEL_BPSK,  /*! BPSK constellation (bps=1) */
        CONSTEL_QPSK,  /*! QPSK constellation (bps=2) */
        CONSTEL_8PSK,  /*! 8PSK constellation (bps=3) */
        CONSTEL_16QAM, /*! 16QAM constellation (bps=4) */
    };

    /*!
     * \brief Make a CDC Symbol Mapper block.
     *
     * \param constel_header    Constellation used for header bytes (BPSK, QPSK,
     *                          8PSK, or 16QAM)
     * \param tsb_tag_key       Output tag key indicating packet/burst duration
     */
    static sptr make(constel_t constel_header = CONSTEL_BPSK,
                     const std::string& tsb_tag_key = "packet_len");

    /*!
     * \brief Returns the constellation used to map header bits.
     */
    virtual constel_t constel_header() const = 0;

    /*!
     * \brief Sets the constellation used to map header bits.
     *
     * \param constel_header    (constel_t) Constellation used for header bits
     *                          (BPSK, QPSK, 8PSK, or 16QAM)
     */
    virtual void set_constel_header(constel_t constel_header) = 0;
};

} // namespace elen90089
} // namespace gr

#endif /* INCLUDED_ELEN90089_SYMBOL_MAPPER_C_H */

# gr-elen90089

GNU Radio out-of-tree (OOT) module for ELEN90089.

This repo contains useful code for completing the CDC design project.
Current components include the following:

- CDC Packet Transmitter

## Installation

Updates will be continuously pushed to this repo throughout semester, so it is
a good idea to make sure you have the latest version installed. If installing
on the ELEN90089 Ubuntu VM for the first time, open a terminal and enter the
following:

```
cd ~/code/source/
git clone https://gitlab.eng.unimelb.edu.au/elen90089-cdc/gr-elen90089.git
cd gr-elen90089/
```

If a clone of the repo already exists on your machine, you can pull updates
from the remote to get the latest changes:

```
git pull
```

The gr-elen90089 OOT module is built using the the standard cmake/make
combination:

```
mkdir build
cd build
cmake ../
make
make test
```

Assuming all unit tests pass, the OOT module can then be installed.

```
sudo make install
```

Note the `sudo` password is that of the *cdc* Ubuntu account.

## Documentation

Doxygen generated documentation is available and can be view by clicking on 
the following link:

- [file:///usr/local/share/doc/gr-elen90089/html/index.html](file:///usr/local/share/doc/gr-elen90089/html/index.html)

or by entering the following in a terminal:

```
firefox /usr/local/share/doc/gr-elen90089/html/index.html
```


import pmt
import time
from threading import Lock

import numpy as np
from gnuradio import gr


class rx_stats(gr.sync_block):
    """Compute throughput (in kbps) of received PDUs."""
    def __init__(self, period=1.0):
        gr.sync_block.__init__(
            self,
            name='Rx Stats',
            in_sig=None,
            out_sig=None)
        self.period = period # output period
        
        # message ports
        self.message_port_register_in(pmt.intern('pdu'))
        self.message_port_register_out(pmt.intern('tput'))
        self.set_msg_handler(pmt.intern('pdu'), self.handle_pdu)
        
        # stats
        self.mutex = Lock()
        self.is_reset = False
        self.n_bytes = 0
        self.t_start = self.t_print = time.time()

    def reset_stats(self, is_reset):
        if is_reset:
            self.mutex.acquire()
            self.is_reset = True
            self.mutex.release()

    def handle_pdu(self, pdu):
        # update stats
        pld = pmt.to_python(pmt.cdr(pdu))
        self.n_bytes += len(pld)
        now = time.time()
        
        # check reset
        self.mutex.acquire()
        is_reset, self.is_reset = self.is_reset, False
        self.mutex.release()
        
        if is_reset:
            self.n_bytes = 0
            self.t_start = self.t_print = now
        
        if now - self.t_print > self.period:
           tput = 8*self.n_bytes / (now - self.t_start) / 1000.0
           self.message_port_pub(pmt.intern('tput'), pmt.from_double(tput))
           self.t_print = now
    

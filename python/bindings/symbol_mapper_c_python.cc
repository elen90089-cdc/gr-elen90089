/*
 * Copyright 2022 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(symbol_mapper_c.h)                                        */
/* BINDTOOL_HEADER_FILE_HASH(b347387bb1dad3898ceb8adbe91a8e32)                     */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <elen90089/symbol_mapper_c.h>
// pydoc.h is automatically generated in the build directory
#include <symbol_mapper_c_pydoc.h>

void bind_symbol_mapper_c(py::module& m)
{

    using symbol_mapper_c = ::gr::elen90089::symbol_mapper_c;

    py::class_<symbol_mapper_c,
               gr::tagged_stream_block,
               gr::block,
               gr::basic_block,
               std::shared_ptr<symbol_mapper_c>> symbol_mapper_c_class(m, "symbol_mapper_c", D(symbol_mapper_c));

    py::enum_<symbol_mapper_c::constel_t>(symbol_mapper_c_class, "constel")
        .value("CONSTEL_BPSK", symbol_mapper_c::CONSTEL_BPSK)
        .value("CONSTEL_QPSK", symbol_mapper_c::CONSTEL_QPSK)
        .value("CONSTEL_8PSK", symbol_mapper_c::CONSTEL_8PSK)
        .value("CONSTEL_16QAM", symbol_mapper_c::CONSTEL_16QAM)
        .export_values();

    py::implicitly_convertible<int, symbol_mapper_c::constel_t>();

    symbol_mapper_c_class
        .def(py::init(&symbol_mapper_c::make),
             py::arg("constel_header") = symbol_mapper_c::constel_t::CONSTEL_BPSK,
             py::arg("tsb_tag_key") = "packet_len",
             D(symbol_mapper_c,make))

        .def("constel_header",
             &symbol_mapper_c::constel_header,
             D(symbol_mapper_c, constel_header))

        .def("set_constel_header",
             &symbol_mapper_c::set_constel_header,
             py::arg("constel_header"),
             D(symbol_mapper_c, set_constel_header));
}

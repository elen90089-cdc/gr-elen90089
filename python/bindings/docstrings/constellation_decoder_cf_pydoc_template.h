/*
 * Copyright 2022 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr,elen90089, __VA_ARGS__ )
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


 
 static const char *__doc_gr_elen90089_constellation_decoder_cf = R"doc()doc";


 static const char *__doc_gr_elen90089_constellation_decoder_cf_constellation_decoder_cf_0 = R"doc()doc";


 static const char *__doc_gr_elen90089_constellation_decoder_cf_constellation_decoder_cf_1 = R"doc()doc";


 static const char *__doc_gr_elen90089_constellation_decoder_cf_make = R"doc()doc";


 static const char *__doc_gr_elen90089_constellation_decoder_cf_bps = R"doc()doc";


 static const char *__doc_gr_elen90089_constellation_decoder_cf_set_bps = R"doc()doc";


 static const char *__doc_gr_elen90089_constellation_decoder_cf_soft_decisions = R"doc()doc";


 static const char *__doc_gr_elen90089_constellation_decoder_cf_set_soft_decisions = R"doc()doc";

  

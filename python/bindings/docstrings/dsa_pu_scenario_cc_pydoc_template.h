/*
 * Copyright 2022 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr,elen90089, __VA_ARGS__ )
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


 
 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_dsa_pu_scenario_cc_0 = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_dsa_pu_scenario_cc_1 = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_make = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_set_scenario = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_get_scenario = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_set_random = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_get_random = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_set_duration_ms = R"doc()doc";


 static const char *__doc_gr_elen90089_dsa_pu_scenario_cc_get_duration_ms = R"doc()doc";

  

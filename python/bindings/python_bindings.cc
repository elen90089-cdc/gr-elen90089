/*
 * Copyright 2020 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#include <pybind11/pybind11.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

namespace py = pybind11;

// Headers for binding functions
/**************************************/
// The following comment block is used for
// gr_modtool to insert function prototypes
// Please do not delete
/**************************************/
// BINDING_FUNCTION_PROTOTYPES(
    void bind_corr_est_cc(py::module& m);
    void bind_moe_symbol_sync_cc(py::module& m);
    void bind_symbol_mapper_c(py::module& m);
    void bind_header_format_cdc(py::module& m);
    void bind_constellation_decoder_cf(py::module& m);
    void bind_costas_loop_cc(py::module& m);
    void bind_dsa_pu_scenario_cc(py::module& m);
// ) END BINDING_FUNCTION_PROTOTYPES


// We need this hack because import_array() returns NULL
// for newer Python versions.
// This function is also necessary because it ensures access to the C API
// and removes a warning.
void* init_numpy()
{
    import_array();
    return NULL;
}

PYBIND11_MODULE(elen90089_python, m)
{
    // Initialize the numpy C API
    // (otherwise we will see segmentation faults)
    init_numpy();

    // Allow access to base block methods
    py::module::import("gnuradio.gr");
    py::module::import("gnuradio.digital");

    /**************************************/
    // The following comment block is used for
    // gr_modtool to insert binding function calls
    // Please do not delete
    /**************************************/
    // BINDING_FUNCTION_CALLS(
    bind_corr_est_cc(m);
    bind_moe_symbol_sync_cc(m);
    bind_symbol_mapper_c(m);
    bind_header_format_cdc(m);
    bind_constellation_decoder_cf(m);
    bind_costas_loop_cc(m);
    bind_dsa_pu_scenario_cc(m);
    // ) END BINDING_FUNCTION_CALLS
}

/*
 * Copyright 2022 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(corr_est_cc.h)                                             */
/* BINDTOOL_HEADER_FILE_HASH(ac063d9b66211941075ac20b1222f1da)                     */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <elen90089/corr_est_cc.h>
// pydoc.h is automatically generated in the build directory
#include <corr_est_cc_pydoc.h>

void bind_corr_est_cc(py::module& m)
{
    using corr_est_cc = ::gr::elen90089::corr_est_cc;

    py::class_<corr_est_cc,
               gr::sync_block,
               gr::block,
               gr::basic_block,
               std::shared_ptr<corr_est_cc>>(m, "corr_est_cc", D(corr_est_cc))

        .def(py::init(&corr_est_cc::make),
             py::arg("sequence"),
             py::arg("threshold") = 0.5,
             py::arg("mark_delay") = 0,
             D(corr_est_cc, make))

        .def("sequence",
             &corr_est_cc::sequence,       
             D(corr_est_cc, sequence))

        .def("set_sequence",
             &corr_est_cc::set_sequence,       
             py::arg("sequence"),
             D(corr_est_cc, set_sequence))

        .def("threshold",
             &corr_est_cc::threshold,       
             D(corr_est_cc, threshold))

        .def("set_threshold",
             &corr_est_cc::set_threshold,
             py::arg("threshold"),
             D(corr_est_cc, set_threshold))

        .def("mark_delay",
             &corr_est_cc::mark_delay,
             D(corr_est_cc, mark_delay))

        .def("set_mark_delay",
             &corr_est_cc::set_mark_delay,       
             py::arg("mark_delay"),
             D(corr_est_cc, set_mark_delay));

}


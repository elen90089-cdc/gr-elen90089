#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 University of Melbourne.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
import numpy as np

from gnuradio import gr, gr_unittest
from gnuradio import blocks
# from gnuradio import blocks
try:
    from elen90089 import dsa_pu_scenario_cc
except ImportError:
    import os
    import sys
    dirname, filename = os.path.split(os.path.abspath(__file__))
    sys.path.append(os.path.join(dirname, "bindings"))
    from elen90089 import dsa_pu_scenario_cc

class qa_dsa_pu_scenario_cc(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    def test_instance(self):
        instance = dsa_pu_scenario_cc(
            scenario = 0,
            random = False,
            seed = 1,
            samp_rate = 32000,
            duration_ms = 10)

    def test_001_randomized_scenario(self):
        scenario = 0
        randomize = True
        seed = 1
        samp_rate = 1000
        dur_ms = 10
        src_data = np.ones(4*10, dtype=np.complex)
        
        # set up fg
        src = blocks.vector_source_c(src_data)
        dut = dsa_pu_scenario_cc(scenario, randomize, seed, samp_rate, dur_ms)
        dst = blocks.vector_sink_c()
        self.tb.connect(src, dut, dst)
        self.tb.run()

        # check data
        expected = np.array(
            [0,]*10 + [0,]*10 + [1,]*10 + [0,]*10,
            dtype=np.complex)
        results = dst.data()
        self.assertFloatTuplesAlmostEqual(results, expected)


if __name__ == '__main__':
    gr_unittest.run(qa_dsa_pu_scenario_cc)

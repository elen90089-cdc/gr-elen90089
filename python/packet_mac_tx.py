#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 University of Melbourne.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

import numpy as np
from gnuradio import gr
import pmt


class packet_mac_tx(gr.sync_block):
    """CDC Packet MAC Transmitter."""
    def __init__(self, bps=1, freq=None, gain=None):
        gr.sync_block.__init__(
            self,
            name="CDC Packet MAC Tx",
            in_sig=None,
            out_sig=None)
        
        # PHY configuration parameters
        self.bps = bps
        self.freq = freq
        self.gain = gain
        
        # create message ports
        self.message_port_register_in(pmt.intern('mac_sdu'))
        self.message_port_register_out(pmt.intern('mac_pdu'))
        self.set_msg_handler(pmt.intern('mac_sdu'), self.handle_sdu)
    
    def set_bps(self, bps):
        self.bps = int(bps)
    
    def set_freq(self, freq):
        self.freq = float(freq)
    
    def set_gain(self, gain):
        self.gain = float(gain)
    
    def handle_sdu(self, sdu):
        """Handle SDU received from layer 3."""
        
        # add MAC header
        data = pmt.to_python(pmt.cdr(sdu))
        header = self.create_header()
        payload = np.concatenate([header, data])
        
        # create phy configuration
        phy_config = self.create_phy_config()
        
        # send PDU to PHY
        pdu = pmt.cons(phy_config, pmt.to_pmt(payload))
        self.message_port_pub(pmt.intern('mac_pdu'), pdu)
    
    def create_header(self):
        """Create MAC header."""
        header = np.array([], dtype=np.uint8)
        return header
    
    def create_phy_config(self):
        """Create PHY configuration dictionary."""
        # Tx commands to bladeRF
        tx_cmd = pmt.make_dict()
        has_cmd = False
        if self.freq is not None:
            tx_cmd = pmt.dict_add(tx_cmd,
                                  pmt.intern('freq'),
                                  pmt.from_double(self.freq))
            self.freq = None
            has_cmd = True
        if self.gain is not None:
            tx_cmd = pmt.dict_add(tx_cmd,
                                  pmt.intern('gain'),
                                  pmt.from_double(self.gain))
            self.gain = None
            has_cmd = True
        
        # create config
        config = pmt.make_dict()
        config = pmt.dict_add(config,
                              pmt.intern('bps'),
                              pmt.from_long(self.bps))
        if has_cmd:
            config = pmt.dict_add(config,
                                  pmt.intern('tx_command'),
                                  tx_cmd)
        
        return config

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 University of Melbourne.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks, digital
import pmt
import numpy as np
import time
try:
    from elen90089 import constellation_decoder_cf
except ImportError:
    import os
    import sys
    dirname, filename = os.path.split(os.path.abspath(__file__))
    sys.path.append(os.path.join(dirname, "bindings"))
    from elen90089 import constellation_decoder_cf


def modulate_u8vector(cnst, vec):
    bits = np.unpackbits(vec)
    bps = cnst.bits_per_symbol()
    
    symb = []
    for ii in range(0, len(bits), bps):
        x = np.flip(bits[ii:ii+bps])
        s = np.packbits(x, bitorder='little')[0]
        symb.append(cnst.points()[s])

    return np.array(symb, dtype=np.complex)

class qa_constellation_decoder_cf(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    def test_instance(self):
        instance = constellation_decoder_cf(1, True, "payload symbols")

    def test_001_constel_change(self):
        # test parameters
        default_bps = 3
        soft_decisions = True
        length_tag = "payload symbols"
        nbytes = 16
        constel = [digital.constellation_calcdist( # normalized qpsk
                       [-1-1j, 1-1j, -1+1j, 1+1j],
                       [0, 1, 2, 3], 4, 1,
                       digital.constellation.normalization.POWER_NORMALIZATION),
                   digital.constellation_bpsk(),
                   digital.constellation_16qam()]

        # set up flowgraph
        src = blocks.pdu_to_tagged_stream(blocks.complex_t, length_tag)
        dec = constellation_decoder_cf(default_bps, soft_decisions, length_tag)
        dst = blocks.vector_sink_f()
        self.tb.connect(src, dec, dst)
        dbg = blocks.tag_debug(gr.sizeof_float, "", length_tag)
        dbg.set_save_all(True)
        dbg.set_display(False)
        self.tb.connect(dec, dbg)

        # create test pdus
        data = np.array([], dtype=np.uint8)
        for c in constel:
            # create modulated vector
            d = np.random.randint(256, size=nbytes, dtype=np.uint8)
            x = modulate_u8vector(c, d)
            # convert to pdu
            bps = pmt.from_long(c.bits_per_symbol())
            meta = pmt.make_dict()
            meta = pmt.dict_add(meta, pmt.intern('bps'), bps)
            pdu = pmt.cons(meta, pmt.init_c32vector(len(x), x))
            # pass to flowgraph
            src.to_basic_block()._post(pmt.intern('pdus'), pdu)
            data = np.concatenate((data, d))
        
        # run fg
        self.tb.start()
        time.sleep(1)
        self.tb.stop()
        
        # check soft bits
        results = np.array(dst.data())
        vec = np.packbits(results > 0)
        self.assertTupleEqual(tuple(data), tuple(vec))
        
        # check length tag is updated
        tags = dbg.current_tags()
        self.assertEqual(len(tags), len(constel))
        offset = 0
        for tag in tags:
            value = pmt.to_python(tag.value)
            self.assertEqual(offset, tag.offset)
            self.assertEqual(8*nbytes, value)
            offset += value


if __name__ == '__main__':
    gr_unittest.run(qa_constellation_decoder_cf)

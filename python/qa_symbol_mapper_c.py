#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 University of Melbourne.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks, digital
import pmt
from time import sleep
import numpy as np
try:
    from elen90089 import symbol_mapper_c
except ImportError:
    import os
    import sys
    dirname, filename = os.path.split(os.path.abspath(__file__))
    sys.path.append(os.path.join(dirname, "bindings"))
    from elen90089 import symbol_mapper_c


def modulate_u8vector(cnst, vec):
    bits = np.unpackbits(vec)
    bps = cnst.bits_per_symbol()
    
    symb = []
    for ii in range(0, len(bits), bps):
        x = np.flip(bits[ii:ii+bps])
        s = np.packbits(x, bitorder='little')[0]
        symb.append(cnst.points()[s])

    return symb


def create_pdu(bps_pld, vec):
    meta = pmt.make_dict();
    meta = pmt.dict_add(meta, pmt.intern('bps'), pmt.from_long(bps_pld))
    pld = pmt.init_u8vector(len(vec), vec)
    
    return pmt.cons(meta, pld)


class qa_symbol_mapper_c(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    def test_001_qpsk(self):
        data_hdr = np.random.randint(256, size=(2,), dtype=np.uint8)
        data_pld = np.random.randint(256, size=(4,), dtype=np.uint8)
        cnst_hdr = digital.constellation_bpsk()
        cnst_pld = digital.constellation_calcdist( # normalized QPSK
            [-1-1j, 1-1j, -1+1j, 1+1j],
            [0x0, 0x2, 0x3, 0x1],
            4,
            1,
            digital.constellation.normalization.POWER_NORMALIZATION)
        expected_result = modulate_u8vector(cnst_hdr, data_hdr)
        expected_result += modulate_u8vector(cnst_pld, data_pld)
        # create and run flowgraph
        mapper = symbol_mapper_c(symbol_mapper_c.constel.CONSTEL_BPSK)
        mapper.to_basic_block()._post(
            pmt.intern('hdr'),
            create_pdu(cnst_pld.bits_per_symbol(), data_hdr))
        mapper.to_basic_block()._post(
            pmt.intern('pld'),
            create_pdu(cnst_pld.bits_per_symbol(), data_pld))
        dst = blocks.vector_sink_c()
        self.tb.connect(mapper, dst)
        self.tb.start()
        sleep(1) # wait for flowgraph to complete
        self.tb.stop()
        # check data
        actual_result = dst.data()
        self.assertEqual(len(actual_result), len(expected_result))
        self.assertFloatTuplesAlmostEqual(actual_result, expected_result)

    def test_002_16qam(self):
        data_hdr = np.random.randint(256, size=(2,), dtype=np.uint8)
        data_pld = np.random.randint(256, size=(4,), dtype=np.uint8)
        cnst_hdr = digital.constellation_bpsk()
        cnst_pld = digital.constellation_16qam()
        expected_result = modulate_u8vector(cnst_hdr, data_hdr)
        expected_result += modulate_u8vector(cnst_pld, data_pld)
        # create and run flowgraph
        mapper = symbol_mapper_c(symbol_mapper_c.constel.CONSTEL_BPSK)
        mapper.to_basic_block()._post(
            pmt.intern('hdr'),
            create_pdu(cnst_pld.bits_per_symbol(), data_hdr))
        mapper.to_basic_block()._post(
            pmt.intern('pld'),
            create_pdu(cnst_pld.bits_per_symbol(), data_pld))
        dst = blocks.vector_sink_c()
        self.tb.connect(mapper, dst)
        self.tb.start()
        sleep(1) # wait for flowgraph to complete
        self.tb.stop()
        # check data
        actual_result = dst.data()
        self.assertEqual(len(actual_result), len(expected_result))
        self.assertFloatTuplesAlmostEqual(actual_result, expected_result)


if __name__ == '__main__':
    gr_unittest.run(qa_symbol_mapper_c)

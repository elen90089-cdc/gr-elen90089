#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 University of Melbourne.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
try:
    from elen90089 import moe_symbol_sync_cc
except ImportError:
    import os
    import sys
    dirname, filename = os.path.split(os.path.abspath(__file__))
    sys.path.append(os.path.join(dirname, "bindings"))
    from elen90089 import moe_symbol_sync_cc

class qa_moe_symbol_sync_cc(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    def test_001_moe_symbol_sync_cc(self):
        sps, nsymbols = 4, 2
        src_data = (1, 2, 3, 1,) * 4
        expected_result = ((1+0j), (1+0j), (3+0j), (3+0j))
        src = blocks.vector_source_c(src_data)
        moe = moe_symbol_sync_cc(sps, nsymbols)
        dst = blocks.vector_sink_c()
        
        self.tb.connect(src, moe, dst)
        self.tb.run()
        
        actual_result = dst.data()
        #print('expected_result', expected_result)
        #print('actual_result', actual_result)
        self.assertEqual(len(actual_result), len(src_data)//sps)
        self.assertFloatTuplesAlmostEqual(expected_result, actual_result)

if __name__ == '__main__':
    gr_unittest.run(qa_moe_symbol_sync_cc)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 University of Melbourne.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import pmt
import numpy as np
try:
    from elen90089 import corr_est_cc
except ImportError:
    import os
    import sys
    dirname, filename = os.path.split(os.path.abspath(__file__))
    sys.path.append(os.path.join(dirname, "bindings"))
    from elen90089 import corr_est_cc


class qa_corr_est_cc(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    def test_001_sequence_detection(self):
        """Test basic sequence detection of correlator."""
        pad = (0,) * 16
        code = (1, 0, 1, 1, 1, 0, 1, 1)
        code_sym = [(2.0*x - 1.0) for x in code] 
        src_bits = pad + code + pad 
        src_data = [(2.0*x - 1.0) for x in src_bits]
        # build flowgraph
        src = blocks.vector_source_c(src_data)
        cor = corr_est_cc(code_sym, threshold=0.5)
        dst = blocks.tag_debug(gr.sizeof_gr_complex, "", "corr_start")
        dst.set_display(False)
        self.tb.connect(src, cor, dst)
        self.tb.run()
        # check test results
        result_data = dst.current_tags()
        index = len(pad) + len(code) - 1
        self.assertEqual(len(result_data), 1)
        self.assertEqual(result_data[0].offset, index)
        corr_mag = pmt.to_python(result_data[0].value)
        self.assertAlmostEqual(corr_mag, 1.0, places=6)

    #def test_002_frequency_estimate(self):
    #    """Test coarse frequency offset estimate."""
    #    phase_inc = 2*np.pi*0.02 # freq offset
    #    pad = (0,) * 16
    #    code = (1, 0, 1, 1, 1, 0, 1, 1)
    #    code_sym = [(2.0*x - 1.0) for x in code] 
    #    src_bits = pad + code + pad 
    #    src_data = [(2.0*x - 1.0) for x in src_bits]
    #    # build flowgraph
    #    src = blocks.vector_source_c(src_data)
    #    rot = blocks.rotator_cc(phase_inc)
    #    cor = corr_est_cc(code_sym, threshold=0.5)
    #    dst = blocks.tag_debug(gr.sizeof_gr_complex, "", "freq_est")
    #    dst.set_display(False)
    #    self.tb.connect(src, rot, cor, dst)
    #    self.tb.run()
    #    # check test results
    #    result_data = dst.current_tags()
    #    index = len(pad) + len(code) - 1
    #    self.assertEqual(len(result_data), 1)
    #    self.assertEqual(result_data[0].offset, index)
    #    freq_est = pmt.to_python(result_data[0].value)
    #    self.assertAlmostEqual(freq_est, phase_inc, places=6)

if __name__ == '__main__':
    gr_unittest.run(qa_corr_est_cc)
